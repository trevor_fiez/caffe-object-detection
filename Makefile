CXXFLAGS =	-O2 -g -Wall -fmessage-length=0

SOURCE = src/

OBJS =	

INCLUDES =  -I/scratch/software/caffe/include -I/scratch/software/installed/include -I/scratch/cuda_7.0.28/include
#-I/scratch/workspace/picStrucWR/src/alglib  -I/scratch/workspace/picStrucWR/src/libsvm

LIBS = /scratch/software/caffe/.build_release/lib/libcaffe.so -L/scratch/cuda_7.0.28/lib64 -L/scratch/software/installed/lib 
TARGET =	test_errors

#$(TARGET):	$(OBJS)
$(TARGET):	
	$(CXX) -fPIC $(INCLUDES) *.cpp  $(LIBS) -o $(TARGET) $(OBJS)

all:	
	rm -f $(OBJS) $(TARGET)
	$(CXX) -fPIC $(INCLUDES) *.cpp  $(LIBS) -o $(TARGET) $(OBJS) 

clean:
	rm -f $(OBJS) $(TARGET)
