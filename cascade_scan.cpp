
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <fstream>
#include <caffe/caffe.hpp>
#include "caffe_classify.hpp"

#include <sys/stat.h>
#include <sys/types.h>

using std::string;
using namespace std;
using namespace cv;
using namespace caffe; 

//void groupRectangles(vector<Rect>& rectList, int groupThreshold, double eps);

class CaffeClassifier
{
	public:
		CaffeClassifier( const string& model_file,
             const string& trained_file,
             const string& mean_file,
             const string& label_file);
		bool detectSingleScale(const Mat& image, int stripCount, Size processingRectSize,
                                           int stripSize, int yStep, double factor, vector<Rect>& candidates);
        void detectMultiScale( const Mat& image, vector<Rect>& objects,
                                          double scaleFactor, int minNeighbors,
                                          int flags, Size minObjectSize, Size maxObjectSize);
		int runAt(Point2i pt);
		
		int runAt(Point2i pt, Mat &mask);
		Size getOriginalWindowSize();
		void generate_detection_images(Mat image, vector<Rect> &objects, int minNeighbors);
		void generate_heat_map(vector<Rect> &candidates);
		//void set_image(Mat image);
		
	public:
		Classifier *classifier;
		Mat grayImage;
		Size scan_window;
		Size origWinSize;
		Mat scaledImage;
		Mat heat_map;
		Mat all_detections;
		Mat clean_detections;
};

Size CaffeClassifier::getOriginalWindowSize()
{
	return scan_window;
}

CaffeClassifier::CaffeClassifier(const string& model_file,
             const string& trained_file,
             const string& mean_file,
             const string& label_file)
{
	classifier = new Classifier(model_file, trained_file, mean_file, label_file);
	scan_window = classifier->mean_size();
	origWinSize = Size(-1, -1);
}
/*
void CaffeClassifier::set_image(Mat image)
{
	cvtColor(image, gray_image, CV_BGR2GRAY);
}*/

int CaffeClassifier::runAt(Point2i pt)
{
	Rect sub_image_area(pt.x , pt.y, scan_window.width, scan_window.height);
	//cout << sub_image_area << endl;
	//cout << grayImage.size() << endl;
	Mat sub_image = scaledImage(sub_image_area);
	
	vector<Prediction> preds = classifier->Classify(sub_image, 1);
	//cout << "checking!" << endl;
	if (preds[0].first.compare("1") == 0)
		return 1;
	else
		return 0;
}

int CaffeClassifier::runAt(Point2i pt, Mat &mask)
{
	Rect sub_image_area(pt.x , pt.y, scan_window.width, scan_window.height);
	//cout << sub_image_area << endl;
	//cout << grayImage.size() << endl;
	Mat sub_image = scaledImage(sub_image_area);
	
	vector<Prediction> preds = classifier->Classify(sub_image, 1);
	//cout << "checking!" << endl;
	int val = 0;
	int return_val = 0;
	if (preds.size() == 0) {
		cout << "Failed at pt: " << pt << endl;
		return 0;
	}
		
	if (preds[0].first.compare("1") == 0) {
		return_val = 1;
		val = (int) (255 * preds[0].second);
		mask.at<char>(pt) = 1;
	} else
		val = (int) (255 - (255 * preds[0].second));
	
	
	return return_val;
}

class CaffeClassifierInvoker : public ParallelLoopBody
{
	
public:
	CaffeClassifierInvoker(CaffeClassifier &_cc, Size _sz1, int _stripSize, 
							int _yStep, double _factor, vector<Rect>& _vec, const Mat& _mask, Mutex* _mtx, Mat &h_mask)
	{
	classifier = &_cc;
	processingRectSize = _sz1;
	stripSize = _stripSize;
	yStep = _yStep;
	scalingFactor = _factor;
	rectangles = &_vec;
	mask = _mask;
	mtx = _mtx;
	heat_mask = h_mask;
	}
	
	void operator()(const Range& range) const
	{
		Size winSize(cvRound(classifier->scan_window.width * scalingFactor), cvRound(classifier->scan_window.height * scalingFactor));
        int y1 = range.start * stripSize;
        int y2 = min(range.end * stripSize, processingRectSize.height);
		//cout << y1 << " " << y2 << " " << classifier->scaledImage.rows <<  endl;
		//cout << processingRectSize.width << " " << classifier->scaledImage.cols << endl;
		
        //Mat temp_mask = Mat::zeros(classifier->origWinSize, CV_8U);
        for( int y = y1; y < y2; y += yStep )
        {
            for( int x = 0; x < processingRectSize.width; x += yStep )
            {
                
                if ( (!mask.empty()) && (mask.at<uchar>(Point(x,y))==0)) {
                    continue;
                }

                int result = classifier->runAt(Point(x, y));
				
				if( result > 0 )
                {
                    mtx->lock();
                    rectangles->push_back(Rect(cvRound(x / scalingFactor), cvRound(y / scalingFactor),
                                               cvRound(winSize.width / scalingFactor), cvRound(winSize.height / scalingFactor)));
                    mtx->unlock();
                }
			}
		}
		
		//imwrite("Heatmap.jpg", temp_mask);
	}
	
	CaffeClassifier * classifier;
	vector<Rect>* rectangles;
    Size processingRectSize;
    int stripSize, yStep;
    double scalingFactor;
    Mat mask;
    Mutex* mtx;
    Mat heat_mask;

};


bool CaffeClassifier::detectSingleScale( const Mat& image, int stripCount, Size processingRectSize,
                                           int stripSize, int yStep, double factor, vector<Rect>& candidates)
{
	Mat currentMask = Mat::ones(scaledImage.size(), CV_8U);
	Mat heat_mask = Mat::zeros(scaledImage.size(), CV_8U);
    vector<Rect> candidatesVector;
    vector<int> rejectLevels;
    vector<double> levelWeights;
    Mutex mtx;
    cout << "Factor: " << factor << endl;
    parallel_for_(Range(0, stripCount), CaffeClassifierInvoker( *this, processingRectSize, stripSize, yStep, factor,
            candidatesVector, currentMask, &mtx, heat_mask));
    
    

	
    candidates.insert( candidates.end(), candidatesVector.begin(), candidatesVector.end() );
    
    return true;
}

void group_rects(vector<Rect>& rectList, int groupThreshold, double eps, vector<int>* weights, vector<double>* levelWeights)
{
    if( groupThreshold <= 0 || rectList.empty() )
    {
        if( weights )
        {
            size_t i, sz = rectList.size();
            weights->resize(sz);
            for( i = 0; i < sz; i++ )
                (*weights)[i] = 1;
        }
        return;
    }

    vector<int> labels;
    int nclasses = partition(rectList, labels, SimilarRects(eps));

    vector<Rect> rrects(nclasses);
    vector<int> rweights(nclasses, 0);
    vector<int> rejectLevels(nclasses, 0);
    vector<double> rejectWeights(nclasses, DBL_MIN);
    int i, j, nlabels = (int)labels.size();
    for( i = 0; i < nlabels; i++ )
    {
        int cls = labels[i];
        rrects[cls].x += rectList[i].x;
        rrects[cls].y += rectList[i].y;
        rrects[cls].width += rectList[i].width;
        rrects[cls].height += rectList[i].height;
        rweights[cls]++;
    }
    if ( levelWeights && weights && !weights->empty() && !levelWeights->empty() )
    {
        for( i = 0; i < nlabels; i++ )
        {
            int cls = labels[i];
            if( (*weights)[i] > rejectLevels[cls] )
            {
                rejectLevels[cls] = (*weights)[i];
                rejectWeights[cls] = (*levelWeights)[i];
            }
            else if( ( (*weights)[i] == rejectLevels[cls] ) && ( (*levelWeights)[i] > rejectWeights[cls] ) )
                rejectWeights[cls] = (*levelWeights)[i];
        }
    }

    for( i = 0; i < nclasses; i++ )
    {
        Rect r = rrects[i];
        float s = 1.f/rweights[i];
        rrects[i] = Rect(saturate_cast<int>(r.x*s),
             saturate_cast<int>(r.y*s),
             saturate_cast<int>(r.width*s),
             saturate_cast<int>(r.height*s));
    }

    rectList.clear();
    if( weights )
        weights->clear();
    if( levelWeights )
        levelWeights->clear();

    for( i = 0; i < nclasses; i++ )
    {
        Rect r1 = rrects[i];
        int n1 = levelWeights ? rejectLevels[i] : rweights[i];
        double w1 = rejectWeights[i];
        if( n1 <= groupThreshold )
            continue;
        // filter out small face rectangles inside large rectangles
        for( j = 0; j < nclasses; j++ )
        {
            int n2 = rweights[j];

            if( j == i || n2 <= groupThreshold )
                continue;
            Rect r2 = rrects[j];

            int dx = saturate_cast<int>( r2.width * eps );
            int dy = saturate_cast<int>( r2.height * eps );

            if( i != j &&
                r1.x >= r2.x - dx &&
                r1.y >= r2.y - dy &&
                r1.x + r1.width <= r2.x + r2.width + dx &&
                r1.y + r1.height <= r2.y + r2.height + dy &&
                (n2 > std::max(3, n1) || n1 < 3) )
                break;
        }

        if( j == nclasses )
        {
            rectList.push_back(r1);
            if( weights )
                weights->push_back(n1);
            if( levelWeights )
                levelWeights->push_back(w1);
        }
    }
}

void CaffeClassifier::generate_heat_map(vector<Rect> &candidates)
{
	
	Mat sum_mask = Mat::zeros(grayImage.size(), CV_32FC1);
    Size scale_image_size = grayImage.size();
    
    Mat detection_mask = Mat::zeros(grayImage.size(), CV_32FC1);
    
    cout << "Cand vec: " << candidates.size() << endl;
    for (int i = 0; i < candidates.size(); i++) {
		detection_mask.at<float>(candidates[i].y + (candidates[i].height / 2), candidates[i].x + (candidates[i].width / 2)) += 1;
	}
    cout << "Done adding detection mask" << endl;
    for (int i = 0; i < scale_image_size.height; i++) {
		for (int j = 0; j < scale_image_size.width; j++) {
			sum_mask.at<float>(i, j) = detection_mask.at<float>(i, j);
			
			if (j > 0)
				sum_mask.at<float>(i, j) += sum_mask.at<float>(i, j - 1);
			
			if (i > 0)
				sum_mask.at<float>(i, j) += sum_mask.at<float>(i - 1, j);
			
			if (i > 0 && j > 0)
				sum_mask.at<float>(i, j) -= sum_mask.at<float>(i - 1, j - 1);
		}
	}
	cout << "Done with sum mask" << endl;
	Mat total_score = Mat::zeros(grayImage.size(), CV_32FC1);
	int middle_x = scan_window.width / 2;
	int middle_y = scan_window.height / 2;
	for (int i = 0; i < scale_image_size.height - scan_window.height; i++) {
		for (int j = 0; j < scale_image_size.width - scan_window.width; j++) {
			int x = j + middle_x;
			int y = i + middle_y;
			total_score.at<float>(i + middle_y, j + middle_x) = sum_mask.at<float>(i + scan_window.height - 1, j + scan_window.width - 1);
			
			if (i > 0)
				total_score.at<float>(y, x) -= sum_mask.at<float>(i - 1, j + scan_window.width - 1);
				
			if (j > 0)
				total_score.at<float>(y, x) -= sum_mask.at<float>(i + scan_window.height - 1, j - 1);
			
			if (i > 0 && j > 0)
				total_score.at<float>(y, x) += sum_mask.at<float>(i - 1, j - 1);
		}
	}
	
	cout << "Done with total score" << endl;

	float max_val = 0;
	for (int i = 0; i < scale_image_size.height; i++) {
		for (int j = 0; j < scale_image_size.width; j++) {
			if (total_score.at<float>(i, j) > max_val)
				max_val = total_score.at<float>(i, j);
		}
	}
	
	Mat final_image = Mat::zeros(grayImage.size(), CV_8U);
	
	for (int i = 0; i < scale_image_size.height; i++) {
		for (int j = 0; j < scale_image_size.width; j++) {
			final_image.at<char>(i, j) = (char) (255 * total_score.at<float>(i, j) / max_val);
		}
	}
	
	heat_map = final_image;
}

void CaffeClassifier::generate_detection_images(Mat image, vector<Rect> &objects, int minNeighbors)
{
	all_detections = image.clone();
	
	for (int i = 0; i < objects.size(); i++) {
		rectangle(all_detections, objects[i], CV_RGB(255, 165, 0), 2, 8, 0);
	}
	
	clean_detections = image.clone();
	
	group_rects( objects, minNeighbors, 0.2, 0, 0);
		
	for (int i = 0 ; i < objects.size(); i++) {
		rectangle(clean_detections, objects[i], CV_RGB(255, 165, 0), 2, 8, 0);
	}
}
	

void CaffeClassifier::detectMultiScale( const Mat& image, vector<Rect>& objects,
                                          double scaleFactor, int minNeighbors,
                                          int flags, Size minObjectSize, Size maxObjectSize)
{
	CV_Assert( scaleFactor > 1 && image.depth() == CV_8U );
	
	objects.clear();
	
	if (origWinSize.width == -1)
		origWinSize = image.size();
	
	 if( maxObjectSize.height == 0 || maxObjectSize.width == 0 )
        maxObjectSize = image.size();
        
	grayImage = image;
    if( grayImage.channels() > 1 )
    {
        Mat temp;
        cvtColor(grayImage, temp, CV_BGR2GRAY);
        grayImage = temp;
    }
    
    Mat imageBuffer(image.rows + 1, image.cols + 1, CV_8U);
    vector<Rect> candidates;

    for( double factor = 1; ; factor *= scaleFactor )
    {
		cout << "Factor: " << factor << endl;
		Size originalWindowSize = getOriginalWindowSize();

        Size windowSize( cvRound(originalWindowSize.width / factor), cvRound(originalWindowSize.height / factor) );
        Size scaledImageSize( cvRound( grayImage.cols *factor ), cvRound( grayImage.rows*factor ) );
        Size processingRectSize( scaledImageSize.width - originalWindowSize.width - 1, scaledImageSize.height - originalWindowSize.height - 1);
		/*
		cout << "Beginning loop" << endl;
		cout << factor << endl;
		cout << originalWindowSize << endl;
		cout << windowSize << endl;
		cout << processingRectSize << endl;
        */
        
        cout << "Window size: " <<  windowSize << endl;
        if( processingRectSize.width <= 0 || processingRectSize.height <= 0 )
            break;
        
        if( windowSize.width < minObjectSize.width || windowSize.height < minObjectSize.height )
            break;
        if( windowSize.width > maxObjectSize.width || windowSize.height > maxObjectSize.height )
            continue;

        scaledImage = Mat::zeros( scaledImageSize, CV_8U);

        resize( grayImage, scaledImage, scaledImageSize, 0, 0, CV_INTER_LINEAR );
		
        int yStep = 10;

        int stripCount, stripSize;

        const int PTS_PER_THREAD = 1000;
        stripCount = ((processingRectSize.width/yStep)*(processingRectSize.height + yStep-1)/yStep + PTS_PER_THREAD/2)/PTS_PER_THREAD;
        stripCount = std::min(std::max(stripCount, 1), 100);
        stripSize = (((processingRectSize.height + stripCount - 1)/stripCount + yStep-1)/yStep)*yStep;
        
        cout << " " << stripCount << " " << stripSize << endl;
		cout << "Detecting single scale" << endl;
        if( !detectSingleScale( scaledImage, stripCount, processingRectSize, stripSize, yStep, factor, candidates) )
            break;

        cout << "Done with single scale" << endl;

    }

	generate_heat_map(candidates);
	
	objects.resize(candidates.size());
    std::copy(candidates.begin(), candidates.end(), objects.begin());
	
	generate_detection_images(image, objects, minNeighbors);

}

vector<string> get_files_in_dir()
{
	FILE *p_fd;
	char buf[100];
	vector<string> file_list;
		
	p_fd = popen("ls", "r");
	if (p_fd == NULL) {
		write(1, "popen failed\n", strlen("popen failed\n"));
		exit(1);
	} 
	
	int read_size = fscanf(p_fd, "%s", buf);
	
	while (read_size != -1) {
		

		string temp(buf);
			
		if (temp.compare(temp.size() - 3, 3, "mp4") == 0) {
		
			file_list.push_back(temp);
		}
		
		memset(buf, '\0', 100);
		
		read_size = fscanf(p_fd, "%s", buf);

	}
	
	pclose(p_fd);
	
	return file_list;
}

void detect_players(CaffeClassifier &player_detector, string vid_name, int overwrite_flag)
{
	string vid_prefix = vid_name.substr(0, vid_name.length() - 4);
	
	if (mkdir(vid_prefix.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0 && overwrite_flag == 0)
			return;
			
	VideoCapture input_vid(vid_name);
	
	int max_frame = input_vid.get(CV_CAP_PROP_FRAME_COUNT);
	
	int cur_frame = 0;
	Mat frame;
	input_vid >> frame;
	
	while (cur_frame < max_frame && cur_frame < 400) {
		
		
		
		vector<Rect> detections;
		
		player_detector.detectMultiScale(frame, detections, 1.2, 7, 0, Size(40, 40), Size(120, 120));
		
		stringstream ss;
		
		ss << cur_frame;
		

		
		string heat_name = "./" + vid_prefix + "/" + ss.str() + "_heat_map.jpg";
		
		string all_name = "./" + vid_prefix + "/" + ss.str() + "_all_detections.jpg";
		
		string clean_name = "./" + vid_prefix + "/" + ss.str() + "_clean_detections.jpg";
		
		if (access(heat_name.c_str(), F_OK) != 0 || overwrite_flag)
			imwrite(heat_name, player_detector.heat_map);
		
		if (access(all_name.c_str(), F_OK) != 0 || overwrite_flag)
			imwrite(all_name, player_detector.all_detections);
		
		if (access(clean_name.c_str(), F_OK) != 0 || overwrite_flag)
			imwrite(clean_name, player_detector.clean_detections);
		
		cout << "Finished frame " << cur_frame << endl;
		for (int i = 0; i < 60; i++) {
			cur_frame += 1;
			if (cur_frame >= max_frame)
				return;
			input_vid >> frame;
		}
	}
}
		


int main(int argc, char** argv) {
	if (argc != 3) {
		std::cerr << "Usage: " << argv[0]
			<< " config_file.txt root_folder" << endl;
			//<< " deploy.prototxt network.caffemodel"
			//<< " mean.binaryproto labels.txt test_image_file root_folder" << std::endl;
		return 1;
	}
	
	int overwrite_flag = 1;

	::google::InitGoogleLogging(argv[0]);

	ifstream config_file(argv[1]);
	string model_file, trained_file, mean_file, label_file;
	
	config_file >> model_file;
	config_file >> trained_file;
	config_file >> mean_file;
	config_file >> label_file;

	CaffeClassifier player_detector(model_file, trained_file, mean_file, label_file);
	
	//VideoCapture input_vid("/scratch/tfiez/videos/hudl/Game08_video0036.avi");
	
	char cwd[100];
	
	memset(cwd, '\0', 100);
	
	getcwd(cwd, 100);
	
	chdir(argv[2]);
	
	vector<string> file_names = get_files_in_dir();
	
	for (int i = 0; i < file_names.size(); i++) {
		cout << "classifying: " << file_names[i] << endl;
		detect_players(player_detector, file_names[i], overwrite_flag);
		cout << "finished: " << file_names[i] << endl;
	}

	/*
	Mat test_image;
	input_vid >> test_image;
	
	vector<Rect> detections;

	player_detector.detectMultiScale( test_image, detections, 1.2, 7, 0, Size(40, 40), Size(120, 120));

	for (int i = 0; i < detections.size(); i++) {
		rectangle(test_image, detections[i], CV_RGB(255, 165, 0), 2, 8, 0);
	}
	
	imwrite("detections.jpg", test_image);*/
	
	chdir(cwd);
	
	return 1;	
}
	
	


